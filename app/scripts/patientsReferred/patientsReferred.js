'use strict'

angular.module('SBS_Consultant_Panel').controller('patientsReferred', ['$scope','$state', '$http', "$location", "$stateParams", "$window", "$rootScope", '$log','UserApiCalling','httpCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, httpCalling, UserQueryApiCalling) {

	$scope.page_count = 1
    $scope.thing_to_search = "";
    $scope.search = "";
    $scope.array_of_patientsReferred = []
    $scope.array_of_classes = []
    $scope.total_patientsReferred_count = 1
    $scope.total_allpatient_count = 0
    $scope.selected_patientStatus = "all" 
    $scope.selected_hospital = null   
    $scope.tableRowExpanded = false;
    $scope.tableRowIndexExpandedCurr = "";
    $scope.tableRowIndexExpandedPrev = "";
    $scope.nameExpanded = "";

    $scope.call_patientStatus_api = function(){
        $scope.activate_loading()
        console.log($rootScope.user.token, $scope.selected_patientStatus)
        var x = $scope.selected_patientStatus
        console.log(x)              
        var send_data = {
            "token": $rootScope.user.token,            
            "section": x
        }
        console.log(send_data)
        var myDataPromise = httpCalling.apiCalling('POST', '/api/vpatient_consultant/patient_visit_response_list/', send_data);       
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)
            if (data.result) {
                $scope.array_of_patientsReferred = data.data;
                $scope.array_of_hospital_referred = data.data;
            
                $scope.max_number_of_pages = parseInt(data.count/10)+1
                console.log($scope.max_number_of_pages, "$scope.max_number_of_pages")
                if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
                    $scope.disable_next = false
                    if ($scope.page_count>1) {
                        $scope.disable_prev = false
                    }else if($scope.page_count==1){
                        $scope.disable_prev = true
                    }
                }else if($scope.max_number_of_pages==1){
                    $scope.disable_prev = true
                    $scope.disable_next = true
                }else if($scope.page_count==1){
                    $scope.disable_prev = true
                }else if ($scope.page_count==$scope.max_number_of_pages) {
                    $scope.disable_next = true
                    $scope.disable_prev = false
                }
                $rootScope.array_of_patientsReferred = data.results                
                $scope.total_allpatient_count = data.count
                $scope.array_of_hospital_referred = data.data
            }           
        })
    }

    $scope.hospital_selection = function(){
        $scope.activate_loading()
        console.log($rootScope.user.token, $scope.selected_patientStatus)
        var x = $scope.selected_patientStatus
        console.log(x)              
        var send_data = {
            "token": $rootScope.user.token,            
            "gp_referred_patient_visit_id": $rootScope.gp_referred_patient_visit_id,
            "hospital_referred_id": $scope.hospital_referred_id
        }
        console.log(send_data)
        var myDataPromise = httpCalling.apiCalling('POST', '/api/vpatient_consultant/consultant_hospital_selection/', send_data);       
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)                    
        })
    }

    $scope.notify_selection = function(){
        $scope.activate_loading()
        console.log($rootScope.user.token, $scope.selected_patientStatus)
        var x = $scope.selected_patientStatus
        console.log(x)              
        var send_data = {
            "token": $rootScope.user.token,            
            "gp_referred_patient_visit_id": $rootScope.gp_referred_patient_visit_id,            
        }
        console.log(send_data)
        var myDataPromise = httpCalling.apiCalling('POST', '/api/vpatient_consultant/notify_hospital/', send_data);       
        myDataPromise.then(function(result) {  
            var data = result.data;
            $scope.deactivate_loading()
            console.log(data)                    
        })
    }

    $scope.select_hospital = function(hospital_selected){
        if (hospital_selected == null) {
            $scope.selected_hospital = null
        }else{
            $scope.selected_hospital = hospital_selected 
        }
        $scope.call_patientStatus_api()
    }

    $scope.dayDataCollapseFn = function () {
        $scope.dayDataCollapse = [];
        for (var i = 0; i < $scope.array_of_patientsReferred.length; i += 1) {
            $scope.dayDataCollapse.push(false);
        }
    };
    
    $scope.selectTableRow = function (index, name) {
        if (typeof $scope.dayDataCollapse === 'undefined') {
            $scope.dayDataCollapseFn();
        }
        if ($scope.tableRowExpanded === false && $scope.tableRowIndexExpandedCurr === "" && $scope.nameExpanded === "") {
            $scope.tableRowIndexExpandedPrev = "";
            $scope.tableRowExpanded = true;
            $scope.tableRowIndexExpandedCurr = index;
            $scope.nameExpanded = name;
            $scope.dayDataCollapse[index] = true;
        } else if ($scope.tableRowExpanded === true) {
            if ($scope.tableRowIndexExpandedCurr === index && $scope.nameExpanded === name) {
                $scope.tableRowExpanded = false;
                $scope.tableRowIndexExpandedCurr = "";
                $scope.nameExpanded = "";
                $scope.dayDataCollapse[index] = false;
            } else {
                $scope.tableRowIndexExpandedPrev = $scope.tableRowIndexExpandedCurr;
                $scope.tableRowIndexExpandedCurr = index;
                $scope.nameExpanded = name;
                $scope.dayDataCollapse[$scope.tableRowIndexExpandedPrev] = false;
                $scope.dayDataCollapse[$scope.tableRowIndexExpandedCurr] = true;
            }
        }
    };
   
    $scope.search_function = function(name){
        $scope.thing_to_search = $scope.search
        $scope.call_patientStatus_api()
        var array_of_patientsReferred = $filter('filter');
    }

    $scope.getNumber = function(num) {
        return new Array(num);   
    }
    
    $scope.pagination_count_update = function(count){
        console.debug(count, $scope.page_count)
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        console.debug(count, $scope.page_count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
           
            $scope.call_patientStatus_api()
        }
    }

    $scope.updateOpenStatus = function(){
        $scope.isOpen = $scope.groups.some(function(item){
            return item.isOpen;
        });
    }
    $scope.page_count_change = function(number, segment_function){
        $scope.page_count = number
        $scope[segment_function]()
    }



    $scope.patient_confirmed_init = function(){
        $scope.selected_patientStatus = "confirmed"
        console.log($rootScope.user, $rootScope.user_present)
        if ($rootScope.user_present){
            console.log($rootScope.user)          
          $scope.call_patientStatus_api()
          $scope.notify_selection()
        }
    }

    $scope.patient_converted_init = function(){
        $scope.selected_patientStatus = "converted"
        console.log($rootScope.user, $rootScope.user_present)
        if ($rootScope.user_present){
            console.log($rootScope.user)          
          $scope.call_patientStatus_api()
        }
    }

    $scope.patient_rejected_init = function(){
        $scope.selected_patientStatus = "rejected"
        console.log($rootScope.user, $rootScope.user_present)
        if ($rootScope.user_present){
            console.log($rootScope.user)          
          $scope.call_patientStatus_api()
        }
    }

    $scope.patient_toRespond_init = function(){
        $scope.selected_patientStatus = "to_be_responded"
        console.log($rootScope.user, $rootScope.user_present)
        if ($rootScope.user_present){
            console.log($rootScope.user)          
          $scope.call_patientStatus_api()
          $scope.hospital_selection()

        }
    }

    $scope.$on('loggedin', function(event, args) {
  		$scope.call_patientStatus_api()
    });

     
}])


