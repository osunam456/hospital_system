'use strict'

angular.module('SBS_Consultant_Panel').controller('profile', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'httpCalling', 'UserQueryApiCalling', '$filter', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, httpCalling, UserQueryApiCalling, $filter) {

	$scope.particular_profile = {}
    $scope.date_date =  new Date();   
    

    $scope.reset_mobile = function(){
    $('#otpBox').modal('show');
    }


  $scope.change_number = function(){
    var send_data = {
      "token" : $rootScope.user.token,
      "new_mobile" : $scope.change_number.mobile
    }
    console.log(send_data)
    $scope.activate_loading()
    var myDataPromise = httpCalling.apiCalling('POST', '/api/profile_consultant/change_number/', send_data);
    myDataPromise.then(function(result) { 


      var data = result.data;
      $scope.deactivate_loading()
      if (data.result == 1) {
        $('#otpVeri').modal('show');
      
      
      }else{
        $scope.errors_to_show(data.errors, 2)
        $('#redirectBox').modal('show');
        $('#otpBox').modal('hide');
      }
    })
  }

  $scope.user_verify = function(){
    var local_storage_data = $window.localStorage.getItem('data')
    if(local_storage_data){
      local_storage_data = JSON.parse(local_storage_data)
      $rootScope.user.mobile = local_storage_data.consultant.mobile;
      $rootScope.user.token = local_storage_data.token;
      var send_data = {
        "mobile" : $rootScope.user.mobile,
        "device_id" : "none",
        "token": $rootScope.user.token
      }
      var myDataPromise = httpCalling.apiCalling('POST', '/api/consultants/verify_consultant/', send_data);
      myDataPromise.then(function(result) {  
        var data = result.data;
        if (data.result == 1) {
          var api_data = data.data
          $rootScope.user_present = true;
          $scope.verifying_user(api_data)
        }else{
          $scope.reset_user_localstorage()
          if ($state.current.name == 'profile/') {
          }else{
            $location.path('profile/')
          }
        }
      })
    }else{
      $scope.reset_user_localstorage()
      if ($state.current.name == 'profile/') {
      }else{
        $location.path('profile/')
      }
    }
  }

  $scope.verify_changed_otp = function(){
    $scope.activate_loading()
    var send_data = {
      "token" : $rootScope.user.token,
      "otp" : $scope.change_number.otp,
      "new_mobile" : $scope.change_number.mobile,
      "old_mobile" : $rootScope.user.mobile
    }
    var myDataPromise = httpCalling.apiCalling('POST', '/api/profile_consultant/verify_changed_number_otp/', send_data);
    myDataPromise.then(function(result) {  
      var data = result.data;
      $scope.deactivate_loading()
      if (data.result == 1) {
        $('#otpVeri').modal('hide');
        $('#otpBox').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        var api_data = data.data
        $rootScope.user_present = true;
        $scope.setting_user(api_data)
       
        $location.path("profile/");
      }else{
        $scope.errors_to_show(data.errors, 1)
        $('#redirectBox').modal('show');
      }
    })
  }



    $scope.get_profiledata = function(){
        $scope.activate_loading()
        console.log($rootScope.user.mobile, $rootScope.user.token, 'lol')        
        var send_data = {
            "mobile" : $rootScope.user.mobile,            
            "token": $rootScope.user.token
        }
        console.log(send_data)
        var myDataPromise = httpCalling.apiCalling('POST', '/api/login_consultant/verify_consultant/', send_data);   
        myDataPromise.then(function(result) { 
            var data = result.data;
            $scope.deactivate_loading()            
            if (data.result) {
                $scope.particular_profile = data.data
                $scope.date_date = new Date($scope.particular_profile.valid_till);
                $scope.date_date = $filter('date')($scope.date_date, 'd MMMM yyyy')           
            }
        })
    }

    
    
	$scope.profile_init = function(){
		if($rootScope.user_present){
			$scope.get_profiledata()
		}else{
		}
    }

    $scope.$on('loggedin', function(event, args) {
		$scope.get_profiledata()
    });

}]);


