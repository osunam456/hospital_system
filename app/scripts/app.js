'use strict';

angular.module('SBS_Consultant_Panel', [
  'ui.router',
  'ngSanitize',
  'ngResource',
  'ngCookies',
  'ngAnimate',
  'ui.bootstrap',
  'ngFileUpload',
  'ui.bootstrap.datetimepicker',
  'angularFileUpload'
])

.config(['$stateProvider' , '$urlRouterProvider', '$locationProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

  $locationProvider.html5Mode(true)
  $locationProvider.hashPrefix('!');

  $urlRouterProvider.otherwise('');
  $urlRouterProvider.when('','/');

  $stateProvider
  .state('Home',{
    url : '/',
    templateUrl : 'views/gp_consultant/login.html',
    controller : 'Home',
  })
  .state('PatientsToRespond',{
    url : 'patients-to-respond/',
    parent : "Home",
    templateUrl : 'views/gp_consultant/patientsReferred/toRespond.html',
    controller : 'patientsReferred',
  })
  // .state('Allpat',{
  //   url : 'allpat/',
  //   parent : "Home",
  //   templateUrl : 'views/gp_consultant/patientsReferred/allpatients.html',
  //   controller : 'patientsReferred',
  // })
  .state('ConfirmedPatient',{
    url : 'confirmed-patients/',
    parent : "Home",
    templateUrl : 'views/gp_consultant/patientsReferred/confirmed.html',
    controller : 'patientsReferred',
  })
  .state('PatientsAdmitted',{
    url : 'patients-admitted/',
    parent : "Home",
    templateUrl : 'views/gp_consultant/patientsReferred/converted.html',
    controller : 'patientsReferred',
  })
  .state('RejectedPatients',{
    url : 'patients-rejected/',
    parent : "Home",
    templateUrl : 'views/gp_consultant/patientsReferred/rejected.html',
    controller : 'patientsReferred',
  })
  .state('Hospitals',{
    url : 'hospitals/',
    parent : "Home",
    templateUrl : 'views/gp_consultant/hospitals/hospitals.html',
    controller : 'hospitals',
  })
  .state('Profile',{
    url : 'profile/',
    parent : "Home",
    templateUrl : 'views/gp_consultant/profile.html',
    controller : 'profile',
  })      
}]);




