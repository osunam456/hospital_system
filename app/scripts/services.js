angular.module('SBS_Consultant_Panel')

.service('httpWithoutDataCalling', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api){
        return $http({
            method : method,
            url : $rootScope.host + api,
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
        .then(function(result){
            return result;
        })
    }
    return { apiCalling: apiCalling };
}])

.service('httpPlainWithoutDataCalling', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api){
        return $http({
            method : method,
            url : api,
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
        .then(function(result){
            return result;
        })
    }
    return { apiCalling: apiCalling };
}])


.service('httpCalling', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api, data){
        return $http({
            method : method,
            url : $rootScope.host + api,
            data : data,
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
        .then(function(result){
          // console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };
}])
.service('UserApiCalling', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api, data, access_token){
        return $http({
            method : method,
            url : $rootScope.host + api,
            data : data,
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            headers: {
              "Authorization": "Token " + access_token,
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
        .then(function(result){
            return result;
        })
    }
    return { apiCalling: apiCalling };
}])
.service("uploadImage",['$http', '$rootScope', "Upload", function($http, $rootScope, Upload){
    var apiCalling = function(method, api, send_data, access_token){
        return Upload.upload({
            method : method,
            url : $rootScope.host + api,
            data : send_data,
            headers: {
                "Authorization": "Token " + access_token,
                "Content-Type": undefined
            }
        })
        .then(function(result){
            // console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };
}])

.service("imageUpload",['$http', '$rootScope', "Upload", function($http, $rootScope, Upload){
    var apiCalling = function(method, api, send_data){
        return Upload.upload({
            method : method,
            url : $rootScope.host + api,
            data : send_data,
            headers: {
                "Content-Type": undefined
            }
        })
        .then(function(result){
            // console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };
}])

.service('UserApiCallingMPF', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api, data, access_token){
        return $http({
            method : method,
            url : $rootScope.host + api,
            data : data,
            headers: {
                "Authorization": "Token " + access_token,
                "Content-Type": "multipart/form-data"
            }
        })
        .then(function(result){
            // console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };
}])


.service('UserQueryApiCalling', ['$http', '$rootScope', function($http, $rootScope) {

    var apiCalling = function(method, api, access_token){
        return $http({
            method : method,
            url : $rootScope.host + api,
            headers: {
                "Authorization": "Token " + access_token,
                "Content-Type": "application/json"
            }
        })
        .then(function(result){
            // console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };
}])

.service("uploadImage",['$http', '$rootScope', "Upload", function($http, $rootScope, Upload){
    var apiCalling = function(method, api, send_data, access_token){
        return Upload.upload({
            method : method,
            url : $rootScope.host + api,
            data : send_data,
            headers: {
                "Authorization": "Token " + access_token,
                "Content-Type": undefined
            }
        })
        .then(function(result){
            // console.log(result)
            return result;
        })
    }
    return { apiCalling: apiCalling };
}])

.filter("trust", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}])