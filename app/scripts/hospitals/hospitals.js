'use strict'

angular.module('SBS_Consultant_Panel').controller('hospitals', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'UserApiCalling', 'httpCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, UserApiCalling, httpCalling, UserQueryApiCalling) {

    $scope.page_count = 1
    $scope.thing_to_search = ""
    $scope.search = ""
    $scope.array_of_hospitals= []
    $scope.total_hospitals_count = 1
    $scope.view = false

    $scope.hospitals_list = function(){
        $scope.activate_loading()
        console.log($scope.page_count, $rootScope.user.token, 'lol')        
        var send_data = {
            "token": $rootScope.user.token,
            "page":  $scope.page_count
        }
        console.log(send_data)
        var myDataPromise = httpCalling.apiCalling('POST', '/api/hospital_consultant/hospital_list/', send_data);   
        myDataPromise.then(function(result) { 
            var data = result.data;
            $scope.deactivate_loading()            
            if (data.result) {
                $scope.array_of_hospitals = data.data;
            
            $scope.max_number_of_pages = parseInt($scope.page_count/10)+1
            if ($scope.max_number_of_pages>1 && $scope.page_count!=$scope.max_number_of_pages) {
                $scope.disable_next = false
                if ($scope.page_count>1) {
                    $scope.disable_prev = false
                }else if($scope.page_count==1){
                    $scope.disable_prev = true
                }
            }else if($scope.max_number_of_pages==1){
                $scope.disable_prev = true
                $scope.disable_next = true
            }else if($scope.page_count==1){
                $scope.disable_prev = true
            }else if ($scope.page_count==$scope.max_number_of_pages) {
                $scope.disable_next = true
                $scope.disable_prev = false
            }

            $rootScope.array_of_hospitals = data.result
            $scope.total_hospitals_count = data.count
        }
        })
    }

    
  

    $scope.hospital_page = function(id){       
        $location.path("/hospital_page/"+id+"/")    
    }

   
    $scope.search_function = function(){
        $scope.thing_to_search = $scope.search
        $scope.hospitals_list()
    }


    $scope.pagination_count_update = function(count, segment_function){
        console.debug(count)
        $scope.page_count = parseInt($scope.page_count) + parseInt(count)
        if ($scope.page_count < 1 ) {
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_next = true
            $scope.disable_prev = false
        }else if($scope.page_count > $scope.max_number_of_pages ){
            $scope.page_count = parseInt($scope.page_count) - parseInt(count)
            $scope.disable_prev = true
            $scope.disable_next = false
        }else{
            $scope[segment_function]()
        }
    }

    $scope.page_count_change = function(number, segment_function){
        $scope.page_count = number
        $scope[segment_function]()
    }

    $scope.hospital_init = function(){
        if($rootScope.user_present){
            $scope.hospitals_list($scope.page_count)
        }
        else{
        }
    }
    

     
    $scope.$on('loggedin', function(event, args) {
        $scope.hospitals_list($scope.page_count)

    });
}])


