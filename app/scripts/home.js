'use strict'

angular.module('SBS_Consultant_Panel').controller('Home', ['$scope','$state', '$http', "$location", "$stateParams", "$window","$rootScope", '$log', 'httpCalling', 'UserApiCalling', 'UserQueryApiCalling', function($scope, $state, $http, $location, $stateParams, $window, $rootScope, $log, httpCalling, UserApiCalling, UserQueryApiCalling) {

  $rootScope.register_user = {}
  $rootScope.errors_to_show = function(message, type){
    $rootScope.message_to_show = []
    if (type==1) {
        $rootScope.message_to_show.push(message)
    }else if(type==2){
        $rootScope.message_to_show=message
    }
    setTimeout(function(){
        $rootScope.message_to_show = []
    }, 2000);
  }

  $rootScope.activate_loading = function(){
    $('.loading_div').css('display', 'block')
  }

  $rootScope.deactivate_loading = function(){
    setTimeout(function() {
      $( ".loading_div" ).css('display','none' );
    }, 2000);
  }

  $rootScope.host = "http://api.sbshealth.com" 

  
  $scope.toTrustedHTML = function( html ){
    return $sce.trustAsHtml( html );
  }
  $scope.contact_details = function(){
    $('#contact_detail').modal('show');
  }

  if (!$rootScope.user_present) {
    $rootScope.user = {}
    $rootScope.user_present = false
  }


  $scope.setting_user = function(api_data){
    $rootScope.user.token = api_data.token
    $rootScope.user.mobile = api_data.consultant.mobile
    $rootScope.user.name = api_data.consultant.name
    $rootScope.user.designation = api_data.consultant.designation
    $rootScope.user.email = api_data.consultant.email
    $rootScope.user.longitude = api_data.consultant.longitude
    $rootScope.user.latitude = api_data.consultant.latitude
    
    $window.localStorage.setItem('data', JSON.stringify(api_data));
    $rootScope.$broadcast("loggedin");        
  }

  $scope.verifying_user = function(api_data, type){
    var local_storage_data = $window.localStorage.getItem('data');
    local_storage_data = JSON.parse(local_storage_data)
    console.log(local_storage_data, api_data)
    $rootScope.user.token = local_storage_data.token    
    $rootScope.user.mobile = api_data.consultant.mobile
    $rootScope.user.name = api_data.consultant.name
    $rootScope.user.designation = api_data.consultant.designation
    $rootScope.user.email = api_data.consultant.email
    $rootScope.user.longitude = api_data.consultant.longitude
    $rootScope.user.latitude = api_data.consultant.latitude
    // $rootScope.user.page = api_data.page     

    local_storage_data.token = $rootScope.user.token;
    local_storage_data.consultant.mobile = $rootScope.user.mobile;
    local_storage_data.consultant.name = $rootScope.user.name;  
    local_storage_data.consultant.designation = $rootScope.user.designation;  
    local_storage_data.consultant.email = $rootScope.user.email;  
    local_storage_data.consultant.longitude = $rootScope.user.longitude;      
    local_storage_data.consultant.latitude = $rootScope.user.latitude;  

    $window.localStorage.setItem('data', JSON.stringify(local_storage_data));
    $rootScope.$broadcast("loggedin");
    console.log($rootScope.user.token)
  }

  $("#mob").keyup(function(event){
    if(event.key == 13){
      $(".buttons_").click();
      e.preventDefault();
    }
  });

  $scope.reset_user_localstorage = function () {
    $window.localStorage.setItem('data', "");
  }        

  $scope.user_login = function(login_from){
    var send_data = {
      "mobile" : $rootScope.user.mobile
    }
    $scope.activate_loading()
    var myDataPromise = httpCalling.apiCalling('POST', '/api/login_consultant/resend_otp/', send_data);
    myDataPromise.then(function(result) {  
      var data = result.data;
      $scope.deactivate_loading()
      if (data.result == 1) {
        $('#otpBox').modal('show');
      }else{
        $scope.errors_to_show(data.errors, 2)
        $('#redirectBox').modal('show');
        $('#otpBox').modal('hide');
      }
    })
  }

  $scope.user_verify = function(){
    var local_storage_data = $window.localStorage.getItem('data')
    if(local_storage_data){
      local_storage_data = JSON.parse(local_storage_data)
      $rootScope.user.mobile = local_storage_data.consultant.mobile;
      $rootScope.user.token = local_storage_data.token;
      var send_data = {
        "mobile" : $rootScope.user.mobile,
        "device_id" : "none",
        "token": $rootScope.user.token
      }
      var myDataPromise = httpCalling.apiCalling('POST', '/api/login_consultant/verify_consultant/', send_data);
      myDataPromise.then(function(result) {  
        var data = result.data;
        if (data.result == 1) {
          var api_data = data.data
          $rootScope.user_present = true;
          $scope.verifying_user(api_data)
          $location.path('/patients-to-respond/')
        }else{
          $scope.reset_user_localstorage()
          if ($state.current.name == 'Home') {
          }else{
            $location.path('/')
          }
        }
      })
    }else{
      $scope.reset_user_localstorage()
      if ($state.current.name == 'Home') {
      }else{
        $location.path('/')
      }
    }
  }

  //end of node
  $scope.verify_user_otp = function(){
    $scope.activate_loading()
    var send_data = {
      "mobile" : $rootScope.user.mobile,
      "otp" : $rootScope.user.otp
    }
    var myDataPromise = httpCalling.apiCalling('POST', '/api/login_consultant/verify_otp/', send_data);
    myDataPromise.then(function(result) {  
      var data = result.data;
      $scope.deactivate_loading()
      if (data.result == 1) {
        $('#otpBox').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        
        var api_data = data.data
        $rootScope.user_present = true;
        $scope.setting_user(api_data)

        $location.path("patientsReferred/");
      }else{
        $scope.errors_to_show(data.errors, 1)
        $('#redirectBox').modal('show');
      }
    })
  }

  $scope.resend_user_otp = function(){
    $scope.activate_loading()
    var send_data = {
      "mobile" : $rootScope.user.mobile
    }
    var myDataPromise = httpCalling.apiCalling('POST', '/api/login_consultant/resend_otp/', send_data);
    myDataPromise.then(function(result) {  
      var data = result.data;
      $scope.deactivate_loading()
      if (data.result == 1) {
        $scope.errors_to_show(data.message, 1)
        $('#redirectBox').modal('show');
      }else{
        $scope.errors_to_show(data.errors, 2)
        $('#redirectBox').modal('show');
      }
    })
  }

  $scope.user_logout = function(){
    $scope.activate_loading()
    var send_data = {
      "token" : $rootScope.user.token
    }
    var myDataPromise = httpCalling.apiCalling('POST', '/api/login_consultant/consultant_logout/', send_data, $rootScope.user.token);
    myDataPromise.then(function(result) {  
      $scope.deactivate_loading()
      var data = result.data;
      if (data.result == 1) {
        $scope.reset_user_localstorage()
        $location.path("/") 
        $window.location.reload();   
      }else{
        $scope.errors_to_show(data.errors, 2)
        $('#redirectBox').modal('show');
      }
    })
  }

  $scope.user_init = function(){
    console.debug("user_init")
    $scope.user_verify()
  } 
}]);


